<?php

declare(strict_types=1);

namespace Drupal\Tests\commerce_checkout_url\Kernel;

use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Drupal\commerce_checkout_url\LinkGenerator;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Tests token integration.
 *
 * @group commerce_checkout_url
 */
class TokenTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'commerce_checkout_url',
  ];

  /**
   * Tests token integration.
   */
  public function testTokenIntegration(): void {
    $this->installConfig('system');

    $settings = Settings::getAll();
    $settings['commerce_checkout_url_private_key'] = $this->randomString();
    new Settings($settings);

    $text = 'Your order is accessible until [commerce-order:commerce-checkout-url-expire] using the following link [commerce-order:commerce-checkout-url]';
    $pattern = '#^Your order is accessible until (.*) using the following link (.*)$#';

    $expectedBase = Url::fromUri('base://checkout-resume/42/')->setAbsolute()->toString();
    $expire = \Drupal::time()->getRequestTime() + LinkGenerator::URL_TTL_DEFAULT;
    $expectedDate = \Drupal::service('date.formatter')->format($expire, 'medium');

    $order = $this->prophesize(OrderInterface::class);
    $order->id()->willReturn(42);
    $order->getCacheContexts()->willReturn([]);
    $order->getCacheTags()->willReturn([]);
    $order->getCacheMaxAge()->willReturn(0);

    $result = \Drupal::token()->replace($text, ['commerce-order' => $order->reveal()]);
    $status = preg_match($pattern, $result, $matches);
    $this->assertSame(1, $status);
    $this->assertSame($expectedDate, $matches[1]);
    $this->assertSame(strpos($matches[2], $expectedBase), 0);

    $expectedDate = \Drupal::service('date.formatter')->format($expire, 'long');

    $order = $this->prophesize(OrderInterface::class);
    $order->id()->willReturn(42);
    $order->getCacheContexts()->willReturn([]);
    $order->getCacheTags()->willReturn([]);
    $order->getCacheMaxAge()->willReturn(0);

    $text = 'Your order is accessible until [commerce-order:commerce-checkout-url-expire:long] using the following link [commerce-order:commerce-checkout-url]';
    $result = \Drupal::token()->replace($text, ['commerce-order' => $order->reveal()]);
    $status = preg_match($pattern, $result, $matches);
    $this->assertSame(1, $status);
    $this->assertSame($expectedDate, $matches[1]);
    $this->assertSame(strpos($matches[2], $expectedBase), 0);
  }

}
