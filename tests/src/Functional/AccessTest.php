<?php

declare(strict_types=1);

namespace Drupal\Tests\commerce_checkout_url\Functional;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\commerce\Traits\CommerceBrowserTestTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\commerce_checkout_url\LinkGeneratorInterface;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\commerce_store\StoreCreationTrait;
use Drupal\user\Entity\Role;

/**
 * Test commerce checkout url claim form.
 *
 * @group commerce_checkout_url
 */
class AccessTest extends BrowserTestBase {
  use StoreCreationTrait;
  use CommerceBrowserTestTrait;
  use UserCreationTrait;

  /**
   * The store entity.
   */
  protected StoreInterface $store;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'field',
    'commerce',
    'commerce_cart',
    'commerce_order',
    'commerce_price',
    'commerce_product',
    'commerce_store',
    'commerce_checkout',
    'commerce_checkout_url',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->store = $this->createStore();
  }

  /**
   * Creates a dummy product for use with other tests.
   */
  public function createDummyOrder(int|string $uid = 0): OrderInterface {
    $variation = ProductVariation::create([
      'type' => 'default',
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => 9.99,
        'currency_code' => 'USD',
      ],
    ]);
    $variation->save();

    $product = Product::create([
      'type' => 'default',
      'title' => 'My product',
      'variations' => [$variation],
      'stores' => [$this->store],
    ]);
    $product->save();

    $orderItem = OrderItem::create([
      'type' => 'default',
      'purchased_entity' => $variation,
    ]);
    $orderItem->save();

    $order = Order::create([
      'type' => 'default',
      'state' => 'draft',
      'order_items' => [$orderItem],
      'store_id' => $this->store->id(),
      'uid' => $uid,
      'cart' => TRUE,
    ]);
    $order->save();

    return $order;
  }

  /**
   * Tests anonymous user accessing an anonymous order.
   */
  public function testAnonymousCheckout(): void {
    $anonymousRole = Role::load(Role::ANONYMOUS_ID);
    assert($anonymousRole !== NULL);
    $anonymousRole->grantPermission('access checkout')->save();

    $order = $this->createDummyOrder(0);

    // Attempt to access an anonymous order directly.
    $this->drupalGet("checkout/{$order->id()}");
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet("checkout-resume/{$order->id()}");
    $this->assertSession()->statusCodeEquals(404);

    // Generate a HMAC protected URL and use that.
    $linkGenerator = \Drupal::service(LinkGeneratorInterface::class);
    assert($linkGenerator instanceof LinkGeneratorInterface);
    $url = $linkGenerator->generate($order);
    $this->assertNotNull($url);
    $this->drupalGet($url->toString());

    // Check response.
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains(new FormattableMarkup('This is a one-time link for %label. The link will expire on', [
      '%label' => $order->label(),
    ]));
    $this->assertSession()->pageTextContains('Click on this button to proceed to checkout.');

    // Submit the form.
    $this->submitForm([], 'Checkout');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertStringEndsWith("checkout/{$order->id()}/login", $this->getSession()->getCurrentUrl());
  }

  /**
   * Tests authenticated user accessing an anonymous order.
   */
  public function testAuthenticatedCheckout(): void {
    $siteUser = $this->setUpCurrentUser(permissions: ['access checkout']);
    $this->drupalLogin($siteUser);

    $order = $this->createDummyOrder(0);

    // Attempt to access an anonymous order directly.
    $this->drupalGet("checkout/{$order->id()}");
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet("checkout-resume/{$order->id()}");
    $this->assertSession()->statusCodeEquals(404);

    // Generate a HMAC protected URL and use that.
    $linkGenerator = \Drupal::service(LinkGeneratorInterface::class);
    assert($linkGenerator instanceof LinkGeneratorInterface);
    $url = $linkGenerator->generate($order);
    $this->assertNotNull($url);
    $this->drupalGet($url->toString());

    // Check response.
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains(new FormattableMarkup('This is a one-time link for %label. The link will expire on', [
      '%label' => $order->label(),
    ]));
    $this->assertSession()->pageTextContains('Click on this button to proceed to checkout.');

    // Submit the form.
    $this->submitForm([], 'Checkout');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertStringEndsWith("checkout/{$order->id()}/order_information", $this->getSession()->getCurrentUrl());
  }

  /**
   * Tests authenticated user accessing its own order.
   */
  public function testOwnOrderCheckout(): void {
    $orderOwner = $this->setUpCurrentUser(permissions: ['access checkout']);
    $this->drupalLogin($orderOwner);

    $ownerId = $orderOwner->id();
    assert($ownerId !== NULL);
    $order = $this->createDummyOrder($ownerId);

    // Attempt to access a non-anonymous order directly.
    $this->drupalGet("checkout/{$order->id()}");
    $this->assertSession()->statusCodeEquals(200);

    $this->drupalGet("checkout-resume/{$order->id()}");
    $this->assertSession()->statusCodeEquals(404);

    // Generate a HMAC protected URL and use that.
    $linkGenerator = \Drupal::service(LinkGeneratorInterface::class);
    assert($linkGenerator instanceof LinkGeneratorInterface);
    $url = $linkGenerator->generate($order);
    $this->assertNotNull($url);
    $this->drupalGet($url->toString());

    // Check response.
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains(new FormattableMarkup('This is a one-time link for %label. The link will expire on', [
      '%label' => $order->label(),
    ]));
    $this->assertSession()->pageTextContains('Click on this button to proceed to checkout.');

    // Submit the form.
    $this->submitForm([], 'Checkout');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertStringEndsWith("checkout/{$order->id()}/order_information", $this->getSession()->getCurrentUrl());
  }

  /**
   * Tests anonymous user attempting to access an order already owned.
   */
  public function testOwnedOrderCheckout(): void {
    $anonymousRole = Role::load(Role::ANONYMOUS_ID);
    assert($anonymousRole !== NULL);
    $anonymousRole->grantPermission('access checkout')->save();

    $orderOwner = $this->setUpCurrentUser(permissions: ['access checkout']);

    $ownerId = $orderOwner->id();
    assert($ownerId !== NULL);
    $order = $this->createDummyOrder($ownerId);

    // Attempt to access an anonymous order directly.
    $this->drupalGet("checkout/{$order->id()}");
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet("checkout-resume/{$order->id()}");
    $this->assertSession()->statusCodeEquals(404);

    // Generate a HMAC protected URL and use that.
    $linkGenerator = \Drupal::service(LinkGeneratorInterface::class);
    assert($linkGenerator instanceof LinkGeneratorInterface);
    $url = $linkGenerator->generate($order);
    $this->assertNotNull($url);
    $this->drupalGet($url->toString());

    // Check response.
    $this->assertSession()->statusCodeEquals(404);

    // Retry with authenticated user.
    $siteUser = $this->setUpCurrentUser(permissions: ['access checkout']);
    $this->drupalLogin($siteUser);

    // Attempt to access an owned order directly.
    $this->drupalGet("checkout/{$order->id()}");
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalGet("checkout-resume/{$order->id()}");
    $this->assertSession()->statusCodeEquals(404);

    // Generate a HMAC protected URL and use that.
    $linkGenerator = \Drupal::service(LinkGeneratorInterface::class);
    assert($linkGenerator instanceof LinkGeneratorInterface);
    $url = $linkGenerator->generate($order);
    $this->assertNotNull($url);
    $this->drupalGet($url->toString());

    // Check response.
    $this->assertSession()->statusCodeEquals(404);
  }

}
