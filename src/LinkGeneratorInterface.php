<?php

declare(strict_types=1);

namespace Drupal\commerce_checkout_url;

use Drupal\Core\Url;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines an interface for a generator of HMAC protected order links.
 */
interface LinkGeneratorInterface {

  /**
   * Generates a HMAC protected URL for the given file.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   A commerce order.
   * @param int $expire
   *   (Optional) The expiry date as a UNIX timestamp.
   *
   * @return \Drupal\Core\Url
   *   The HMAC protected order link.
   */
  public function generate(OrderInterface $order, ?int $expire = NULL): Url;

}
