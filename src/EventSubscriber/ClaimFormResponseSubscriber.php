<?php

declare(strict_types=1);

namespace Drupal\commerce_checkout_url\EventSubscriber;

use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Claim form response subscriber which masks 403 as 404 responses.
 */
final class ClaimFormResponseSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::EXCEPTION => ['onAccessDeniedException'],
    ];
  }

  /**
   * Constructs a claim form response subscriber.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match.
   */
  public function __construct(protected RouteMatchInterface $routeMatch) {
  }

  /**
   * Kernel exception event handler.
   */
  public function onAccessDeniedException(ExceptionEvent $event): void {
    if (
      $event->getThrowable() instanceof AccessDeniedHttpException &&
      $this->routeMatch->getRouteName() === 'commerce_checkout_url.claim_form'
    ) {
      $event->setThrowable(new NotFoundHttpException());
    }
  }

}
