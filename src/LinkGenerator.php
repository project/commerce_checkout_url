<?php

declare(strict_types=1);

namespace Drupal\commerce_checkout_url;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Implements a generator of HMAC protected order links.
 */
class LinkGenerator implements LinkGeneratorInterface {

  /**
   * Default time to live for a checkout URL.
   */
  public const URL_TTL_DEFAULT = 604800;

  /**
   * Constructs a new link generator.
   *
   * @param \Drupal\commerce_checkout_url\SecurityKeyInterface $securityKey
   *   The commerce checkout url security key service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The request time service.
   */
  public function __construct(
    protected SecurityKeyInterface $securityKey,
    protected TimeInterface $time,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function generate(OrderInterface $order, ?int $expire = NULL): Url {
    $orderId = $order->id();
    if ($orderId === NULL) {
      throw new \InvalidArgumentException('Order must have a an order id');
    }

    if (!isset($expire)) {
      $ttl = Settings::get('commerce_checkout_url_ttl');
      $ttl = (int) (is_numeric($ttl) ? $ttl : self::URL_TTL_DEFAULT);
      $expire = $this->time->getRequestTime() + $ttl;
    }
    $expire_string = dechex($expire);

    $hmac = $this->securityKey->generate($orderId, $expire_string);
    return Url::fromRoute('commerce_checkout_url.claim_form', [
      'commerce_order' => $orderId,
      'expire' => $expire_string,
      'hmac' => $hmac,
    ]);
  }

}
