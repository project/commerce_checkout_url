<?php

declare(strict_types=1);

namespace Drupal\commerce_checkout_url\Form;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\OrderAssignmentInterface;
use Drupal\user\Entity\User;
use Psr\Container\ContainerInterface;

/**
 * Provides the checkout url claim form.
 */
final class ClaimForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('date.formatter'),
      $container->get('commerce_cart.cart_provider'),
      $container->get('commerce_order.order_assignment')
    );
  }

  /**
   * Constructs a new checkout url claim form.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   *   The date formatter.
   * @param \Drupal\commerce_cart\CartProviderInterface $cartProvider
   *   The commerce cart provider.
   * @param \Drupal\commerce_order\OrderAssignmentInterface $orderAssignment
   *   The order assignment service.
   */
  public function __construct(
    protected DateFormatterInterface $dateFormatter,
    protected CartProviderInterface $cartProvider,
    protected OrderAssignmentInterface $orderAssignment,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'commerce_checkout_url_claim';
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param array<string, mixed> $form
   * @phpstan-return array<string, mixed>
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?OrderInterface $commerce_order = NULL, ?string $expire = NULL): array {
    assert($commerce_order !== NULL);
    assert($expire !== NULL);

    $form_state->set('commerce_order', $commerce_order);

    $form['message'] = [
      '#markup' => $this->t('<p>This is a one-time link for %label. The link will expire on %expiration_date.</p><p>Click on this button to proceed to checkout.</p>', [
        '%label' => $commerce_order->label(),
        '%expiration_date' => $this->dateFormatter->format((int) hexdec($expire)),
      ]),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Checkout'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param array<string, mixed> $form
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $commerce_order = $form_state->get('commerce_order');
    assert($commerce_order instanceof OrderInterface);
    $account = $this->currentUser();

    if (!$account->isAnonymous()) {
      $userEntity = User::load($account->id());
      assert($userEntity !== NULL);
      $this->orderAssignment->assign($commerce_order, $userEntity);
    }
    $this->cartProvider->finalizeCart($commerce_order);

    $form_state->setRedirect('commerce_checkout.form', [
      'commerce_order' => $commerce_order->id(),
    ]);
  }

}
