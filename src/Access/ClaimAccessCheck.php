<?php

declare(strict_types=1);

namespace Drupal\commerce_checkout_url\Access;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\commerce_checkout_url\SecurityKeyInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Psr\Container\ContainerInterface;

/**
 * Checks access for checkout url claim form.
 *
 * This class is designed to be used with '_custom_access' route requirement.
 *
 * @see \Drupal\Core\Access\CustomAccessCheck
 */
class ClaimAccessCheck implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get(SecurityKeyInterface::class)
    );
  }

  /**
   * Constructs a new access check for checkout url claim form.
   *
   * @param \Drupal\commerce_checkout_url\SecurityKeyInterface $securityKey
   *   The security key service.
   */
  public function __construct(
    protected SecurityKeyInterface $securityKey,
  ) {
  }

  /**
   * Checks if the hmac is valid and grants access if so.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The commerce order.
   * @param string $expire
   *   A hex-encoded UNIX timestamp.
   * @param string $hmac
   *   The message authentication code.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function checkAccess(
    OrderInterface $commerce_order,
    string $expire,
    string $hmac,
    AccountInterface $account,
  ) {
    $customer = $commerce_order->getCustomer();
    $orderAccessible =
      $customer->isAnonymous() ||
      $customer->id() === $account->id();

    $orderId = $commerce_order->id();
    assert($orderId !== NULL);
    $hmacValid = $this->securityKey->verify($orderId, $expire, $hmac);

    return AccessResultAllowed::allowedIf($orderAccessible && $hmacValid)->mergeCacheMaxAge(0);
  }

}
