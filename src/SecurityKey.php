<?php

declare(strict_types=1);

namespace Drupal\commerce_checkout_url;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Site\Settings;

/**
 * Provides methods to generate and check the HMAC for commerce checkout urls.
 */
class SecurityKey implements SecurityKeyInterface {

  /**
   * Constructs a download security key service.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The request time service.
   * @param string $hmacKey
   *   The HMAC key. Defaults commerce_checkout_url_private_key setting and
   *   falls back to Drupal private key + hash salt.
   */
  public function __construct(
    protected TimeInterface $time,
    protected ?string $hmacKey = NULL,
  ) {
    if (!isset($this->hmacKey)) {
      $preferredKey = Settings::get('commerce_checkout_url_private_key');
      if (is_string($preferredKey)) {
        $this->hmacKey = $preferredKey;
      }
      else {
        $fallbackKey = \Drupal::service('private_key')->get() . Settings::getHashSalt();
        $this->hmacKey = $fallbackKey;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function verify(string|int $orderId, string $expire, string $hmac): bool {
    $result = FALSE;

    if (strlen($expire) && strlen($hmac)) {
      $calculated_hmac = Crypt::hmacBase64($orderId . $expire, $this->hmacKey);
      $result = hash_equals($calculated_hmac, $hmac) &&
        $this->time->getRequestTime() <= hexdec($expire);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function generate(string|int $orderId, string $expire): string {
    return Crypt::hmacBase64($orderId . $expire, $this->hmacKey);
  }

}
