<?php

declare(strict_types=1);

namespace Drupal\commerce_checkout_url;

/**
 * Provides methods to generate and check the HMAC for commerce checkout urls.
 */
interface SecurityKeyInterface {

  /**
   * Validates a HMAC.
   *
   * @param string|int $orderId
   *   The file uri, e.g., private://ticket-12345.pdf.
   * @param string $expire
   *   A hex-encoded UNIX timestamp.
   * @param string $hmac
   *   The message authentication code.
   *
   * @return bool
   *   TRUE if this download link is still valid, FALSE otherwise.
   */
  public function verify(string|int $orderId, string $expire, string $hmac): bool;

  /**
   * Calculates a base-64 encoded, URL-safe sha-256 HMAC.
   *
   * @param string|int $orderId
   *   Identifier of a commerce order.
   * @param string $expire
   *   A hex-encoded UNIX timestamp.
   *
   * @return string
   *   The message authentication code.
   */
  public function generate(string|int $orderId, string $expire): string;

}
