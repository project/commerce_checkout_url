<?php

/**
 * @file
 * Placeholder replacement tokens for commerce checkout url.
 */

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Site\Settings;
use Drupal\commerce_checkout_url\LinkGenerator;
use Drupal\commerce_checkout_url\LinkGeneratorInterface;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Implements hook_token_info().
 *
 * @phpstan-return array{
 *   tokens: array{
 *     commerce-order: array<string, array{
 *       name: string|\Drupal\Core\StringTranslation\TranslatableMarkup,
 *       description: string|\Drupal\Core\StringTranslation\TranslatableMarkup,
 *     }>,
 *   },
 * }
 */
function commerce_checkout_url_token_info(): array {
  // Tokens for orders.
  $order = [];

  $order['commerce-checkout-url-expire'] = [
    'name' => t('Commerce Checkout URL expiry date'),
    'description' => t('The date when a one-time link to the checkout page generated now will expire.'),
  ];
  $order['commerce-checkout-url'] = [
    'name' => t('Commerce Checkout URL.'),
    'description' => t('A one-time link to the checkout page of an order protected with an authentication code.'),
  ];

  return [
    'tokens' => [
      'commerce-order' => $order,
    ],
  ];
}

/**
 * Implements hook_tokens().
 *
 * @phpstan-param string $type
 * @phpstan-param array<string, string> $tokens
 * @phpstan-param array<string, mixed> $data
 * @phpstan-param array{
 *   langcode?: string,
 * } $options
 * @phpstan-return array<string, mixed>
 */
function commerce_checkout_url_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $token_service = \Drupal::token();

  $url_options = ['absolute' => TRUE];
  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = LanguageInterface::LANGCODE_DEFAULT;
  }
  $replacements = [];

  if (
    $type === 'commerce-order' &&
    isset($data['commerce-order']) &&
    $data['commerce-order'] instanceof OrderInterface
  ) {
    $order = $data['commerce-order'];
    $ttl = Settings::get('commerce_checkout_url_ttl');
    $ttl = (int) (is_numeric($ttl) ? $ttl : LinkGenerator::URL_TTL_DEFAULT);
    $expire = \Drupal::time()->getRequestTime() + $ttl;

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'commerce-checkout-url':
          $linkGenerator = \Drupal::service(LinkGeneratorInterface::class);
          assert($linkGenerator instanceof LinkGeneratorInterface);
          $url = $linkGenerator->generate($order, $expire);
          $replacements[$original] = $url->setOptions($url_options)->toString();
          break;

        case 'commerce-checkout-url-expire':
          $dateFormatter = \Drupal::service(DateFormatterInterface::class);
          assert($dateFormatter instanceof DateFormatterInterface);
          $date_format = DateFormat::load('medium');
          assert($date_format !== NULL);
          $bubbleable_metadata->addCacheableDependency($date_format);
          $replacements[$original] = $dateFormatter->format($expire, 'medium', langcode: $langcode);
          break;
      }
    }

    if ($date_tokens = $token_service->findWithPrefix($tokens, 'commerce-checkout-url-expire')) {
      $replacements += $token_service->generate('date', $date_tokens, ['date' => $expire], $options, $bubbleable_metadata);
    }

  }

  return $replacements;
}
