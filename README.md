INTRODUCTION
------------

Commerce Checkout URL provides a way to generate HMAC protected links to orders
in the checkout process. Such links can be sent by email to customers in order
to transfer ownership of commerce carts prepared programmatically or imported
from remote systems.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/commerce_checkout_url

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/commerce_checkout_url


REQUIREMENTS
------------

This module requires the following modules:

 * Commerce (https://drupal.org/project/commerce)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

The module has no UI for configuration. Use the following settings to tweak its
behavior:

  // Menu item path, defaults to 'checkout-resume'
  $conf['commerce_checkout_url_menu_item_path'] = 'checkout-resume';
  // Private key, defaults to the sites private key and hash salt.
  $conf['commerce_checkout_url_private_key'] = 'el5k9sPcF+ddHoW/07F2aUSNzN...';
  // Validity time of a link in seconds. Defaults to 604800 (1 week).
  $conf['commerce_checkout_url_ttl'] = 3600;
